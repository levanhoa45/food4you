$(function () {
  $("#slider").slick({
    autoplay: false,
    speed: 1000,
    centerMode: true,
    centerPadding: "500px",
    slidesToShow: 1,
    arrows:false,
    lazyLoad: 'ondemand',
    dots: true,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1420,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '340px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 1120,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '260px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 800,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '200px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 620,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '155px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '80px',
          slidesToShow: 1
        }
      }
    ]
  });
});
